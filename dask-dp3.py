#!/usr/bin/env python3

from glob import glob
import casacore.tables
import numpy as np
import dask.array as da 


data_dir = '/var/scratch/offringa/Raw-RFI-Test-Set'
ms_list = sorted(glob(f'{data_dir}/*.MS'))

freq_chunk_sizes = []
freqs = []
for ms in ms_list:
    t = casacore.tables.table(f'{ms}/SPECTRAL_WINDOW')
    freq = t[0]['CHAN_FREQ']
    freq_chunk_sizes.append(len(freq))
    freqs.append(freq)

freqs = np.concatenate(freqs)    

ms0 = ms_list[0]
time_table = casacore.tables.taql("SELECT UNIQUE TIME from $ms0")
times = time_table.getcol("TIME")

print(freq_chunk_sizes)

print(len(freqs))
print(len(times))

def read_freq(ms_list, block_id=None):
    t = casacore.tables.table(f'{ms_list[block_id[0]]}/SPECTRAL_WINDOW')
    freq = t[0]['CHAN_FREQ']
    return freq

def read_data(ms_list, block_info=None):
    pass

x1 = da.map_blocks(
    read_freq,
    ms_list,
    dtype=np.float64,
    chunks=(tuple(freq_chunk_sizes), ), meta=np.array((), dtype=np.float64))



print(x1.compute())    
